var mongoose = require("mongoose");
var fs = require("fs");
var Patrimony = require("../models/Patrimony.js");
var eventController = require("./Events.js");
var Event = require("../models/Events.js");
var sharp = require("sharp");
var path = require("path");

var patrimonyController = {};

patrimonyController.formCreate = function (req, res) {
  res.render("../views/template", {
    loadContent: {
      page: "../views/patrimony/createPatrimony.ejs",
      error: false,
    },
  });
};

patrimonyController.formCreateError = function (req, res) {
  res.render("../views/template", {
    loadContent: {
      page: "../views/patrimony/createPatrimony.ejs",
      error: true,
    },
  });
};

patrimonyController.saveImage = function (req, res, next) {
  let requestType = req.url.split("/")[1];
  requestType = requestType.charAt(0).toUpperCase() + requestType.slice(1);

  // If there is no file in the request, the creation of the patrimony is canceled and the user is redirected to the form to create a patrimony
  if (!req.file) {
    if (requestType == "Edit") {
      req.noNewImage = true;
      return next();
    } else {
      return res.redirect("/admin/patrimony/formCreate");
    }
  } else {
    // If the image uploaded is not in the correct format, we return an error to the user
    if (
      req.file.mimetype != "image/jpeg" &&
      req.file.mimetype != "image/png" &&
      req.file.mimetype != "image/jpg"
    ) {
      return res.redirect("/admin/patrimony/form" + requestType);
    }

    //debugger;
    // Gets the file extension of the image to add it to the name of the file
    var fileExtension = req.file.mimetype.split("/")[1];

    var destionationPath = req.file.destination;
    var patrimonyName = req.body.name;
    var resizedImagePath = path.join(destionationPath, patrimonyName + '.' + fileExtension);

    // Resize the image to 250x250 pixels
    sharp(req.file.path)
      .resize(250, 250)
      .toFile(resizedImagePath, function (err) {
        if (err) {
          //console.log(err);
          return res.redirect("/admin/patrimony/form" + requestType);
        }

        // If resizing the image was successful, update req.body.publicPath with the path of the resized image
        req.body.publicPath = "/images/patrimonies/" + patrimonyName + '.' + fileExtension;

        // Delete the original image
        fs.unlink(req.file.path, function (err) {
          if (err) {
            //console.log(err);
          }
        });

        next();
      });
  };
};

patrimonyController.create = function (req, res) {
  // Gets the number of patrimonies in the database to create a new ID for the patrimony.
  Patrimony.countDocuments({}).then((countPatrimonies) => {
    //debugger;
    //console.log(req.body); // Debugging purposes
    // Creates a new Patrimony with the information from the form, which has already been validated.
    let patrimony = new Patrimony({
      // ID of the patrimony.
      patrimonyId: countPatrimonies + 1,
      // Name of the patrimony.
      name: req.body.name,
      // Patrimony's location.
      location: {
        // Latitude and longitude of the patrimony.
        latitude: req.body.latitude,
        longitude: req.body.longitude,
        // City and district of the patrimony.
        city: req.body.city,
        district: req.body.district,
      },
      // Type of patrimony - Examples: Monument, Museum, etc.
      type: req.body.type,
      // Events related to the patrimony.
      eventsRelated: {
        // Array of ongoing events.
        ongoingEvents: [],
        // Array of past events.
        doneEvents: [],
      },
      // Image of the patrimony - Optional.
      image: {
        // Path of the image.
        publicPath: req.body.publicPath,
        // Type of file - Example: .jpg, .png, etc.
        typeOfFile: req.file.mimetype,
      },
    });

    // Checks if the patrimony already exists in the database.
    Promise.all([
      Patrimony.findOne({ patrimonyId: patrimony.patrimonyId }),
      Patrimony.findOne({ name: patrimony.name }),
    ]).then(([dbPatrimonyId, dbPatrimonyName]) => {
      if (dbPatrimonyId != null || dbPatrimonyName != null) {
        return res.redirect("http://localhost:5000/admin/patrimony/create/error");
      } else {
        // If the patrimony doesn't already exist, it will be saved in the database.
        patrimony.save().then(() => {
          res.redirect("/admin/patrimony");
        });
      }
    });
  });
};

patrimonyController.dashboard = function (req, res) {
  // Find all patrimonies in the database
  Patrimony.find({}).then((dbPatrimonies) => {
    if (dbPatrimonies == null) {
      // If there are no patrimonies in the database, it will return a 500 error indicating that there are no patrimonies in the database.
      return res.status(500).send({
        success: false,
        message: "No patrimonies in the database",
      });
    } else {
      res.render("../views/template", {
        loadContent: {
          page: "../views/patrimony/dashboardPatrimony.ejs",
          patrimonies: dbPatrimonies,
        },
      });
    }
  });
};

patrimonyController.show = function (req, res) {
  // Finds the patrimony with the id passed in the URL.
  Patrimony.findOne({ patrimonyId: req.params.patrimonyId }).then(
    (dbPatrimony) => {
      if (dbPatrimony == null) {
        return res.status(500).send({
          success: false,
          message: "Patrimony not found",
        });
      } else {
        // If no errors are found, it renders the patrimonies/show view --> The render path may be changed in the future when there's a better understand of how the views will be organized.
        res.render("../views/template", {
          loadContent: {
            page: "../views/patrimony/viewPatrimony.ejs",
            patrimony: dbPatrimony,
          },
        });
      }
    }
  );
};

patrimonyController.formEdit = function (req, res) {
  // Finds the patrimony with the id passed in the URL.
  Patrimony.findOne({ patrimonyId: req.params.patrimonyId }).then(function (
    dbPatrimony
  ) {
    if (dbPatrimony == null) {
      //console.log("Error:", err);
    } else {
      // If no errors are found, it renders the patrimonies/edit view --> The render path may be changed in the future when there's a better understand of how the views will be organized.
      res.render("../views/template", {
        loadContent: {
          page: "../views/patrimony/editPatrimony.ejs",
          patrimony: dbPatrimony,
        },
      });
    }
  });
};

patrimonyController.edit = async function (req, res) {
  //debugger;
  // Find the previous patrimony in the database.
  let previousPatrimony = await Patrimony.findOne({ patrimonyId: req.params.patrimonyId });

  debugger;

  // Extract the previous patrimony's events related since the form doesn't have the events related.
  let previousPatrimonyEventsRelated = previousPatrimony.eventsRelated;
  // Extract the previous patrimony's image since the image might not be updated.
  let previousPatrimonyImage = previousPatrimony.image;
  // Get the path of the previous patrimony image
  let previousPatrimonyImagePath = previousPatrimonyImage.publicPath;
  // Get the name of the previous patrimony image so that it can be replaced with the new name
  let previousPatrimonyImageName = previousPatrimonyImagePath.split('/patrimonies/')[1].split('.')[0];
  // Replace the name of the previous ticket image with the new name
  previousPatrimonyImagePath = previousPatrimonyImagePath.replace(previousPatrimonyImageName, req.body.name);
  // Replace the publicPath of the previous ticket image with the new publicPath
  //If the name of the ticket is not changed, the publicPath will remain the same
  previousPatrimonyImage.publicPath = previousPatrimonyImagePath;

  if (req.noNewImage) {
    var newPatrimony = new Patrimony(
      {
        // ID of the patrimony.
        patrimonyId: req.params.patrimonyId,
        // Name of the patrimony.
        name: req.body.name,
        // Patrimony's location.
        location: {
          // Latitude and longitude of the patrimony.
          latitude: req.body.latitude,
          longitude: req.body.longitude,
          // City and district of the patrimony.
          city: req.body.city,
          district: req.body.district,
        },
        // Type of patrimony - Examples: Monument, Museum, etc.
        type: req.body.type,
        // Events related to the patrimony.
        eventsRelated: previousPatrimonyEventsRelated,
        // Image of the patrimony.
        image: previousPatrimonyImage,
      },
      { _id: false }
    );
  } else {
    var newPatrimony = new Patrimony(
      {
        // ID of the patrimony.
        patrimonyId: req.params.patrimonyId,
        // Name of the patrimony.
        name: req.body.name,
        // Patrimony's location.
        location: {
          // Latitude and longitude of the patrimony.
          latitude: req.body.latitude,
          longitude: req.body.longitude,
          // City and district of the patrimony.
          city: req.body.city,
          district: req.body.district,
        },
        // Type of patrimony - Examples: Monument, Museum, etc.
        type: req.body.type,
        // Events related to the patrimony.
        eventsRelated: previousPatrimonyEventsRelated,
        // Image of the patrimony - Optional.
        image: {
          // Path of the image.
          publicPath: req.body.publicPath,
          // Type of file - Example: .jpg, .png, etc.
          typeOfFile: req.file.mimetype,
        },
      },
      { _id: false }
    );
  }

  Patrimony.findOneAndUpdate(
    { patrimonyId: req.params.patrimonyId },
    newPatrimony
  ).then((patrimony) => {
    // If no patrimony is found, return error
    if (patrimony == null) {
      return res.status(500).send({
        success: false,
        message: "Error updating patrimony!",
      });
    } else {
      // If the patrimony is found, rename the image file
      if (req.noNewImage && patrimony.name != newPatrimony.name) {
        fs.renameSync(__dirname + "/../public" + patrimony.image.publicPath, __dirname + "/../public" + newPatrimony.image.publicPath, function (err) {
          if (err) {
            // Handle the error logic here if needed
            // For now, leave it empty since it's doing what it's supposed to do
            // even though it's throwing an error
            // if needed, add a console.log(err) to see the error
            //console.log(err);
          }
        });
      }

      res.status(200).send({ success: true, message: "Patrimony updated" });
    }
  });
};

function checkIfPatrimonyHasEvents(req, res, next) {
  return new Promise((resolve, reject) => {
    // Find the patrimony with the id passed in the URL.
    Patrimony.findOne({ patrimonyId: req.params.patrimonyId }).then(
      (patrimony) => {
        // If the patrimony has events, it will reject the removal of the patrimony.
        if (
          patrimony.eventsRelated.ongoingEvents.length > 0 ||
          patrimony.eventsRelated.doneEvents.length > 0
        ) {
          resolve(true);
        } else {
          resolve(false);
        }
      }
    );
  });
}

async function decrementId(req) {
  // Start a session to handle the transaction
  var session = await Patrimony.startSession();
  // Start a transaction since we're going to be updating multiple documents
  session.startTransaction();
  try {
    // Find all patrimonies
    //with id greater than
    //the id passed in the URL
    var patrimonies = await Patrimony.find({
      patrimonyId: { $gt: req.params.patrimonyId },
    });

    // If no patrimonies are found, throw an error
    if (patrimonies.length == 0) {
      throw new Error(
        "No patrimonies found with id greater than the id passed"
      );
    }

    // Otherwise, decrement the id of each patrimony found
    // and decrement it's reference stored in the events
    // associated to the current patrimony
    patrimonies.forEach(async (patrimony) => {
      await decrementLocationOnAssociatedEvents(patrimony.patrimonyId, session);
      patrimony.patrimonyId = patrimony.patrimonyId - 1;
      await patrimony.save({ session });
    });

    // If no errors are found, commit the transaction
    await session.commitTransaction();
    session.endSession();

    // Return an indication of success
    return Promise.resolve();
  } catch (error) {
    // If any error is found, abort the transaction
    //console.log(error);
    await session.abortTransaction();
    session.endSession();
    // Return an indication of failure
    return Promise.reject(error);
  }
}

async function decrementLocationOnAssociatedEvents(location, session) {
  try {
    let events = await Event.find({ location: location }).session(session);

    if (events.length == 0) {
      throw new Error("No events found with location equal to the id passed");
    }

    events.forEach(async (event) => {
      event.location = event.location - 1;
      await event.save({ session });
    });

    // If no errors are found, return a promise indicating success
    return Promise.resolve();
  } catch (error) {
    //console.log(error);
    return Promise.reject(error);
  }
}

patrimonyController.delete = function (req, res, next) {
  // Call the decrementId function
  // to asynchronously decrement the id of each patrimony
  // with id greater than the id of the patrimony being deleted
  // while the patrimony is being deleted

  // Find the patrimony with the id passed in the URL and delete it
  Promise.all([checkIfPatrimonyHasEvents(req, res, next)])
    .then((result) => {
      if (result[0]) {
        return res.status(500).send({
          success: false,
          message: "Patrimony has events",
        });
      } else {
        Patrimony.findOneAndDelete({
          patrimonyId: req.params.patrimonyId,
        }).then(async (patrimony) => {
          // If no patrimony is found, return error message
          if (patrimony == null) {
            return res.status(500).send({
              success: false,
              message: "Patrimony not found",
            });
          } else {
            // If the patrimony has events, return error message
            await decrementId(req, res, next);
            fs.unlink(
              __dirname + "/../public" + patrimony.image.publicPath,
              function (err) {
                if (err) {
                  //console.log(err);
                } else {
                  //console.log("File removed successfully!");
                }
              }
            );

            res.status(200).send({
              success: true,
              message: "Patrimony deleted",
            });
          }
        });
      }
    })
    .catch(() => {
      return res.status(500).send({
        success: false,
        message: "Patrimony has events",
      });
    });
};

module.exports = patrimonyController;
