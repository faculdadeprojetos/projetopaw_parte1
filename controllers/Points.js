var Points = require("../models/Points");
var pointsController = {};

//check if there is one fidelization program
getFidelizationProgram = () => {
  return new Promise((resolve, reject) => {
    Points.findOne({}).then((fidelizationProgram) => {
      if (fidelizationProgram == null) {
        resolve(false);
      } else {
        resolve(fidelizationProgram);
      }
    });
  });
};

//Show the fidelization program if there is one defined
pointsController.dashboard = async (req, res) => {
  let fidelizationProgram = await getFidelizationProgram();

  if (fidelizationProgram) {
    res.render("../views/template", {
      loadContent: {
        page: "../views/points/dashboardPoints.ejs",
        points: fidelizationProgram,
      },
    });
  } else {
    res.render("../views/template", {
      loadContent: {
        page: "../views/points/dashboardPoints.ejs",
        doesntExist: true,
      },
    });
  }
};

//show form of add fidelization program
pointsController.formCreate = (req, res) => {
  res.render("../views/template", {
    loadContent: {
      page: "../views/points/addPoints.ejs",
      error: false,
    },
  });
};

pointsController.formCreateError = (req, res) => {
  res.render("../views/template", {
    loadContent: {
      page: "../views/points/addPoints.ejs",
      error: true,
    },
  });
};

//add fidelization program
pointsController.create = async (req, res, next) => {
  let fidelizationProgram = await getFidelizationProgram();

  if (fidelizationProgram) {
    return res.redirect("localhost:5000/admin/point/create/error")
  } else {
    var points = new Points(req.body);
    points.save().then(() => {
      res.redirect("/admin/point");
    });
  }
};

//show form for edit fidelization program
pointsController.formEdit = (req, res) => {
  Points.findOne({}).then((pointsInformation) => {
    res.render("../views/template", {
      loadContent: {
        page: "../views/points/editPoints.ejs",
        points: pointsInformation,
      },
    });
  });
};

//edit fidelization program
pointsController.edit = (req, res, next) => {
  Points.findByIdAndUpdate(req.params.id, req.body).then((fidelization) => {
    if (fidelization == null) {
      return res.status(500).json({
        success: false,
        message: "Fidelization program not found",
      });
    } else {
      return res
        .status(200)
        .send({ success: true, message: "Fidelization program updated" });
    }
  });
};

//delete atual fidelization program
pointsController.delete = (req, res, next) => {
  Points.findOneAndDelete({ _id: req.params.id }).then((pointsInformation) => {
    if (pointsInformation == null) {
      return res.status(500).send({
        success: false,
        message: "Fidelization program not found",
      });
    } else {
      res
        .status(200)
        .send({ success: true, message: "Fidelization program deleted" });
    }
  });
};

//get the discount for each points to process the a sale
pointsController.getDiscountForEach100Points = (req, res) => {
  Points.find({}).then((pointsInformation) => {
    if (pointsInformation == null) {
      res.status(400).json({ error: "db searching error" });
    } else {
      res.status(200).json({
        points: pointsInformation[0].discountForEach100Points,
      });
    }
  });
};

module.exports = pointsController;
