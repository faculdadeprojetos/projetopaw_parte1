var mongoose = require("mongoose");
var Event = require("../models/Events.js");
const Patrimony = require("../models/Patrimony.js");
var Ticket = require("../models/Tickets.js");
var axios = require("axios");

var eventController = {};

// Show list of events
eventController.dashboard = function (req, res) {
  Event.find({}).then((dbEvents) => {
    // If no events are found, return error
    if (dbEvents == null) {
      return res.status(500).send({
        success: false,
        message: "Error retrieving events",
      });
    } else {
      // Else, sort the events by wheter they are ongoing or not
      // and render the dashboard with the events

      dbEvents.sort((a, b) => {
        // If a is ongoing and b is not, a should be first
        // If a is not ongoing and b is, b should be first
        // If both are ongoing or both are not ongoing, they should be in the same order
        if (a.finished && !b.finished) {
          return 1;
        } else if (!a.finished && b.finished) {
          return -1;
        } else {
          return 0;
        }
      });

      res.render("../views/template", {
        loadContent: {
          page: "../views/events/dashboardEvent.ejs",
          events: dbEvents,
        },
      });
    }
  });
};

eventController.formCreate = function (req, res) {
  Patrimony.find({}).then((dbPatrimonies) => {
    // If no patrimonies are found, return error
    if (dbPatrimonies == null) {
      return res.status(500).send({
        success: false,
        message: "Error retrieving patrimonies",
      });
    } else {
      // Else, render the create event form with the patrimonies

      // Render the create event form to create a new event
      res.render("../views/template", {
        loadContent: {
          page: "../views/events/createEvent.ejs",
          patrimonies: dbPatrimonies,
        },
      });
    }
  });
};

eventController.formCreateError = function (req, res) {
  Patrimony.find({}).then((dbPatrimonies) => {
    // If no patrimonies are found, return error
    if (dbPatrimonies == null) {
      return res.status(500).send({
        success: false,
        message: "Error retrieving patrimonies",
      });
    } else {
      // Else, render the create event form with the patrimonies
      res.render("../views/template", {
        loadContent: {
          page: "../views/events/createEvent.ejs",
          patrimonies: dbPatrimonies,
          error: true,
        },
      });
    }
  });
};

eventController.formCreatePatrimonyAssociated = function (req, res) {
  Patrimony.find({}).then((dbPatrimonies) => {
    if (dbPatrimonies == null) {
      return res.status(500).send({
        success: false,
        message: "Error retrieving patrimonies",
      });
    } else {
      // Else, render the create event form with the patrimonies
      // and associate the value of the location field with the patrimony's id
      // sent in the url
      res.render("../views/template", {
        loadContent: {
          page: "../views/events/createEventDeterminedPatrimony.ejs",
          patrimonies: dbPatrimonies,
          location: req.params.patrimonyId,
        },
      });
    }
  });
};

async function associateEventToPatrimony(eventId, finished, req, res) {
  // We don't want to wait for the patrimony to be associated to the event
  // so we use a promise
  // This way, the event is created and the user can do other things
  // while the patrimony is being associated to the event in the background
  new Promise((resolve, reject) => {
    // Find patrimony based on the value of the location field
    Patrimony.findOne({ patrimonyId: req.body.location }).then((patrimony) => {
      //console.log(patrimony);
      // If the patrimony is not found, return error
      if (patrimony == null) {
        reject("Error associating event to patrimony - patrimony not found");
      } else {
        if (finished) {
          patrimony.eventsRelated.doneEvents.push(eventId);
        } else {
          patrimony.eventsRelated.ongoingEvents.push(eventId);
        }
        patrimony.save().then(() => {
          resolve();
        });
      }
    });
  });
}

async function removeEventFromPreviousPatrimony(patrimonyId, finished, req) {
  new Promise((resolve, reject) => {
    // Find patrimony based on the value of the location field
    Patrimony.findOne({ patrimonyId: patrimonyId }).then(async (patrimony) => {
      // If the patrimony is not found, return error
      if (patrimony == null) {
        reject("Error removing event from patrimony - patrimony not found");
      } else {
        if (finished) {
          patrimony.eventsRelated.doneEvents.pull(req.params.eventId);
        } else {
          patrimony.eventsRelated.ongoingEvents.pull(req.params.eventId);
        }
        await patrimony.save();
        resolve();
      }
    });
  });
}

eventController.create = function (req, res) {
  debugger;
  // Count the number of events in the database to get the next id for the new event
  Event.countDocuments({}).then((countEvents) => {
    let event = new Event({
      // Id of the new event
      eventId: countEvents + 1,
      // Name of the new event
      name: req.body.name,
      // Description of the new event
      description: req.body.description,
      // Type of the new event
      type: req.body.type,
      // Date of the new event
      date: req.body.date,
      // Time of the new event
      schedule: {
        // Start time of the new event
        start: req.body.start,
        // End time of the new event
        end: req.body.end,
      },
      // Location of the new event - Id of the patrimony where the event is going to happen
      location: req.body.location,
      // Classification of the new event - G, PG, PG-13, R, NC-17
      classification: req.body.classification,
      // Number of seats available
      seatsAvailable: req.body.seatsAvailable,
    });

    // Check if there is already an event with the same id or name
    Promise.all([
      Event.findOne({ eventId: event.eventId }),
      Event.findOne({ name: event.name }),
    ]).then(([dbEventId, dbEventName]) => {
      if (dbEventId != null || dbEventName != null) {
        return res.redirect("http://localhost:5000/admin/event/create/error");
      } else {
        // Else, save the new event in the database
        event.save().then(async () => {
          // After saving the event, associate it to the patrimony
          await associateEventToPatrimony(event.eventId, event.finished, req, res);
          // Redirect to the events page
          return res.redirect("/admin/event");
        });
      }
    });
  });
};

async function markEventAsFinishedOnPatrimony(patrimonyId, req, res) {
  // We don't want to wait for the event to be marked as finished and switched to the finished events
  // so we use a promise
  // This way, the event is marked as finished and the user can do other things
  // while the event is being switched to the finished events of the patrimony in the background
  new Promise((resolve, reject) => {
    // Find patrimony based on the value of the location field
    Patrimony.findOne({ patrimonyId: patrimonyId }).then((patrimony) => {
      // If the patrimony is not found, return error
      if (patrimony == null) {
        reject("Error associating event to patrimony - patrimony not found");
      } else {
        // Else, remove the event from the ongoing events and add it to the finished events
        patrimony.eventsRelated.ongoingEvents.splice(
          patrimony.eventsRelated.ongoingEvents.indexOf(req.params.eventId),
          1
        );
        patrimony.eventsRelated.doneEvents.push(req.params.eventId);
        patrimony.save().then(() => {
          resolve();
        });
      }
    });
  });
}

eventController.markAsFinished = async function (req, res) {
  // Find the event with the id passed in the url
  Event.findOne({ eventId: req.params.eventId }).then((event) => {
    // If no event is found, return error
    if (event == null) {
      return res.status(500).send({
        success: false,
        message: "Error retrieving event",
      });
    } else {
      // Else, mark the event as finished
      event.finished = true;
      event.save().then(async () => {
        // After saving the event, remove it from the patrimony as an ongoing event and add it to the finished events
        // asynchronusly, so the user can do other things while the patrimony is being associated
        await markEventAsFinishedOnPatrimony(event.location, req, res);
        // Redirect to the events dashboard page
        res.redirect("/admin/event");
      });
    }
  });
};

eventController.getEventById = function (req, res) {
  // Find the event with the id passed in the url
  Event.findOne({ eventId: req.params.eventId }).then(async (dbEvent) => {
    let patrimony = await Patrimony.findOne({
      patrimonyId: dbEvent.location,
    });

    // If no event is found, return error
    if (dbEvent == null) {
      return res.status(500).send({
        success: false,
        message: "Error retrieving event",
      });
    } else {
      // Set the location field to the name of the patrimony so
      // we display the name of the patrimony instead of the id
      // which is more readable for the administator making the sale
      let event = dbEvent.toObject();
      // Else, return the event data
      event.location = patrimony.name;
      res.status(200).send({
        event: event,
      });
    }
  });
};

eventController.show = function (req, res) {
  // Find the event with the id passed in the url
  Event.findOne({ eventId: req.params.eventId }).then((dbEvent) => {
    // If no event is found, return error
    if (dbEvent == null) {
      return res.status(500).send({
        success: false,
        message: "Error retrieving event",
      });
    } else {
      // Else, render the event page with the event data
      res.render("../views/template", {
        loadContent: {
          page: "../views/events/showEvent.ejs",
          event: dbEvent,
        },
      });
    }
  });
};

eventController.formEdit = function (req, res) {
  // Find the event with the id passed in the url and render the edit event form
  Event.findOne({ eventId: req.params.eventId }).then((dbEvent) => {
    // If no event is found, return error
    if (dbEvent == null) {
      res.render("../views/template", {
        loadContent: {
          page: "../views/event/editEvent.ejs",
          error: true,
        },
      });
    } else {
      // Find all patrimonies to populate the dropdown
      Patrimony.find({}).then((dbPatrimonies) => {
        // If no patrimonies are found, return error
        if (dbPatrimonies == null) {
          res.render("../views/template", {
            loadContent: {
              page: "../views/event/editEvent.ejs",
              error: true,
            },
          });
        } else {
          // Render the edit event form with the event and patrimonies data
          res.render("../views/template", {
            loadContent: {
              page: "../views/events/editEvent.ejs",
              event: dbEvent,
              patrimonies: dbPatrimonies,
            },
          });
        }
      });
    }
  });
};

eventController.edit = async function (req, res) {
  let previousEvent = await Event.findOne({ eventId: req.params.eventId });
  let previousName = previousEvent.name;
  let previousState = previousEvent.finished;
  // Find the event with the id passed in the url and update it with the new data
  let newEvent = new Event(
    {
      // Id of the event
      eventId: req.params.eventId,
      // Name of the event
      name: req.body.name,
      // Description of the event
      description: req.body.description,
      // Type of the new event
      type: req.body.type,
      // Date of the new event
      date: req.body.date,
      // Time of the new event
      schedule: {
        // Start time of the new event
        start: req.body.start,
        // End time of the new event
        end: req.body.end,
      },
      // Location of the new event - Id of the patrimony where the event is going to happen
      location: req.body.location,
      // Classification of the new event - G, PG, PG-13, R, NC-17
      classification: req.body.classification,
      // Number of seats available
      seatsAvailable: req.body.seatsAvailable,
      // Status of the event - true if the event is finished, false otherwise
      finished: req.body.finished,
    },
    { _id: false }
  );

  Event.findOneAndUpdate({ eventId: req.params.eventId }, newEvent).then(
    async (event) => {
      // If no event is found, return error
      if (event == null) {
        return res.status(500).send({
          success: false,
          message: "Error updating event!",
        });
      } else {
        // If the event is finished, remove it from the patrimony as an ongoing event and add it to the finished events
        if (newEvent.finished != previousState) {
          await associateEventToPatrimony(
            newEvent.eventId,
            newEvent.finished,
            req,
            res
          );
          await removeEventFromPreviousPatrimony(
            event.location,
            event.finished,
            req,
            res
          );
        } else {
          // If the event is not finished, update the patrimony with the new event
          await associateEventToPatrimony(
            newEvent.eventId,
            newEvent.finished,
            req,
            res
          );

          // If the location of the event changed, remove the event from the previous patrimony
          if (newEvent.location != event.location) {
            await removeEventFromPreviousPatrimony(
              event.location,
              event.finished,
              req,
              res
            );
          }
        }

        if (newEvent.name != previousName) {
          await updateTicketsWithNameEqualToEventName(
            previousName,
            newEvent.name,
            req,
            res
          );
        }
        //res.status(200).json(event); // Return the updated event
        res.status(200).send({
          // Return the updated event
          success: true,
          message: "Event edited!",
        });
      }
    }
  );
};

async function updateTicketsWithNameEqualToEventName(previousName, newName, req, res) {
  try {
    debugger;
    // Start a mongoose transaction
    const session = await Event.startSession();
    // Find all the tickets with the name equal to the name of the event to be deleted
    let ticketsToUpdate = await Ticket.find({ name: previousName });

    if (ticketsToUpdate.length == 0) {
      session.endSession();
      return;
    }

    session.startTransaction();

    for (const ticket of ticketsToUpdate) {
      // For each ticket found, send an edit request to update the name of the ticket
      // this is so we don't have to manually re-do the logic of updating the ticket name and file name
      var ObjectToBeSent = new Map();
      ObjectToBeSent.set("ticketId", ticket.ticketId);
      ObjectToBeSent.set("name", newName);
      ObjectToBeSent.set("date", new Date(ticket.date).toISOString().slice(0, 10));
      ObjectToBeSent.set("hour", ticket.hour);
      ObjectToBeSent.set("row", ticket.eventSeat.row);
      ObjectToBeSent.set("seat", ticket.eventSeat.seat);
      ObjectToBeSent.set("type", ticket.type);
      ObjectToBeSent.set("price", ticket.price);
      ObjectToBeSent.set("image", undefined);

      // Example PUT request
      axios.put("http://localhost:5000/admin/ticket/edit/" + ticket.ticketId, Object.fromEntries(ObjectToBeSent), {
        headers: {
          "Content-Type": "application/json",
          "x-access-token": req.cookies.token,
        },
      })
    }

    // Commit the transaction after all the tickets have been updated
    // and end the session
    await session.commitTransaction();
    session.endSession();
  } catch (err) {
    // If an error occurs, abort the transaction and end the session
    //console.log(err);
    await session.abortTransaction();
    session.endSession();
  }
}

async function RemoveTicketsWithNameEqualToEventName(eventName) {
  try {
    debugger;
    // Start a mongoose transaction
    const session = await Ticket.startSession();
    // Find all the tickets with the name equal to the name of the event to be deleted
    let ticketsToRemove = await Ticket.find({ name: eventName });
    let remainingTickets = await Ticket.find({ name: { $ne: eventName } });

    if (ticketsToRemove.length == 0) {
      session.endSession();
      return;
    }

    session.startTransaction();

    // For each ticket to be removed, remove it from the database
    ticketsToRemove.forEach(async (ticket) => {
      // For each ticket found, remove it from the database
      await Ticket.findOneAndDelete({ ticketId: ticket.ticketId });
    });

    // For the remaining tickets, swap their ids to the id of their index in the array + 1
    remainingTickets.forEach(async (ticket) => {
      await Ticket.findOneAndUpdate(
        { _id: ticket._id },
        { ticketId: remainingTickets.indexOf(ticket) + 1 }
      );
    });

    // Commit the transaction
    await session.commitTransaction();
    session.endSession();
  } catch (error) {
    //console.log(error);
    // Abort the transaction since there was an error
    await session.abortTransaction();
    session.endSession();
  }
}

async function decrementId(req) {
  debugger;
  // Start a mongoose transaction since we are going to update multiple documents at once
  let session = await mongoose.startSession();

  try {
    // Find all the events with id greater than the id of the event to be deleted
    // and decrement the id of each event by 1
    let events = await Event.find({ eventId: { $gt: req.params.eventId } });

    if (events.length == 0) {
      Promise.resolve();
    }

    session.startTransaction();

    // For each event found, decrement its id by 1
    // and update it's reference in the patrimony where it is stored
    events.forEach(async (event) => {
      await decrementEventIdOnPatrimony(event.eventId);
      event.eventId = event.eventId - 1;
      await event.save();
    });

    // Commit the transaction
    await session.commitTransaction();
    session.endSession();

    return Promise.resolve();
  } catch (error) {
    //console.log(error);
    // Abort the transaction since there was an error
    await session.abortTransaction();
    session.endSession();
    return Promise.reject();
  }
}

async function decrementEventIdOnPatrimony(eventId) {
  debugger;
  try {
    // Find all the patrimonies affected by the decrement of the event id
    // and decrement the id of the stored refence to the event by 1 (ongoingEvents and doneEvents contain the event id)
    let patrimony = await Patrimony.findOne({
      $or: [
        { "eventsRelated.ongoingEvents": eventId },
        { "eventsRelated.doneEvents": eventId },
      ],
    });

    if (patrimony == null) {
      throw new Error(`No patrimony found with the event id: ${eventId}`);
    }

    if (patrimony.eventsRelated.ongoingEvents.includes(eventId)) {
      patrimony.eventsRelated.ongoingEvents[
        patrimony.eventsRelated.ongoingEvents.indexOf(eventId)
      ] = eventId - 1;
    } else {
      patrimony.eventsRelated.doneEvents[
        patrimony.eventsRelated.doneEvents.indexOf(eventId)
      ] = eventId - 1;
    }

    await patrimony.save();
  } catch (error) {
    // If the error is related to version error in the database, retry the operation
    if (error instanceof mongoose.Error.VersionError) {
      await decrementEventIdOnPatrimony(eventId);
    }
  }
}

eventController.delete = function (req, res, next) {
  // Check if the event has any tickets associated with it
  Event.findOne({ eventId: req.params.eventId }).then(async (event) => {
    if (event == null) {
      return res.status(500).send({
        success: false,
        message: "Error deleting event",
      });
    }

    await RemoveTicketsWithNameEqualToEventName(event.name);

    Event.findOneAndDelete({ eventId: req.params.eventId }).then(
      async (event) => {
        debugger;
        Promise.all([
          removeEventFromPreviousPatrimony(
            event.location,
            event.finished,
            req,
            res
          ),
          decrementId(req),
        ])
          .then(() => {
            return res
              .status(200)
              .send({ success: true, message: "Event deleted" });
          })
          .catch((error) => {
            return res
              .status(500)
              .send({ success: false, message: "Error deleting event" });
          });
      }
    );
  });
};

module.exports = eventController;
