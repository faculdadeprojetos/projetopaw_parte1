var mongoose = require('mongoose');
var Sale = require("../../models/Sales.js");
var Client = require("../../models/Client.js");

var salesStoreController = {};

salesStoreController.clientPhase = function (req, res, next) {
    var clientId = req.clientId;

    Client.findOne({ "personalInformation.clientId": clientId }).then(function (client) {
        if (client == null) {
            return res.status(404).json({ message: "Client not found" });
        } else {
            req.client = client;
            next();
        }
    });
};

salesStoreController.getSalesByClient = function (req, res) {
    Sale.find({ "client.clientId": req.clientId }).then(function (sales) {
        if (sales.length == 0) {
            return res.status(404).json({ message: "No sales made by this client were found in the database" });
        } else {
            return res.status(200).json(sales);
        }
    });
};

module.exports = salesStoreController;

