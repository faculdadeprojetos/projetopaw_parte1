var express = require("express");
var router = express.Router();
var patrimonyController = require("../controllers/Patrimony");
var authController = require("../controllers/Auth");
var multer = require("multer");
var { body, validationResult } = require("express-validator");

var upload = multer({ dest: "public/images/patrimonies" });

router.get(
  // GET the dashboard of the patrimony
  "/",
  authController.verifyToken,
  patrimonyController.dashboard
);

router.get(
  // GET the form to create a new patrimony
  "/formCreate",
  authController.verifyToken,
  patrimonyController.formCreate
);

router.get(
  "/create/error",
  authController.verifyToken,
  patrimonyController.formCreateError
);

router.post(
  // POST the form to create a new patrimony
  "/create",
  upload.single("image"),
  [
    body("name").isLength({ min: 3, max: 50 }).withMessage("Name is required and must be between 3 and 50 characters"),
    body("city").isLength({ min: 3, max: 50 }).withMessage("City is required and must be between 3 and 50 characters"),
    body("type").isIn([
      "Art gallery",
      "Bar/Nightclub",
      "Brewery/Winery",
      "College/University",
      "Community center",
      "Convention center",
      "Conference center",
      "Cruise ship",
      "Historical/Cultural estate",
      "Hotel",
      "Museum/Aquarium/Zoo",
      "Resort",
      "Restaurant",
      "Retreat center",
      "Social club/Lounge",
      "Stadium",
      "Theater",
    ]).withMessage("Type is required"),
  ],
  // Verify if the user is logged in
  authController.verifyToken,
  // If the user is logged in, we save the image in the request
  patrimonyController.saveImage,
  function (req, res, next) {
    var errors = validationResult(req);
    // If there are errors in the validation of the form fields, we return the errors to the user
    if (!errors.isEmpty()) {
      return res.redirect("http://localhost:5000/admin/patrimony/create/error");
    }
    // If the image is saved, we call the method to create the patrimony
    patrimonyController.create(req, res);
  }
);

// GET patrimony details if the user is logged in and the patrimony exists
router.get(
  "/show/:patrimonyId",
  authController.verifyToken,
  patrimonyController.show
);

// GET patrimony id and create a form to edit the patrimony
router.get(
  "/formEdit/:patrimonyId",
  authController.verifyToken,
  // If the user is logged in, we show the form to edit the patrimony in the url
  patrimonyController.formEdit
);

// PUT a patrimony based on the data of the form sent in the request into the database if the user is logged in
router.put(
  "/edit/:patrimonyId",
  upload.single("image"),
  [
    body("name").isLength({ min: 3, max: 50 }).withMessage("Name is required and must be between 3 and 50 characters"),
    body("city").isLength({ min: 3, max: 50 }).withMessage("City is required and must be between 3 and 50 characters"),
    body("type").isIn([
      "Art gallery",
      "Bar/Nightclub",
      "Brewery/Winery",
      "College/University",
      "Community center",
      "Convention center",
      "Conference center",
      "Cruise ship",
      "Historical/Cultural estate",
      "Hotel",
      "Museum/Aquarium/Zoo",
      "Resort",
      "Restaurant",
      "Retreat center",
      "Social club/Lounge",
      "Stadium",
      "Theater",
    ]).withMessage("Type is required"),
  ],
  // Verify if the user is logged in
  authController.verifyToken,
  // If the user is logged in, we save the image in the request
  patrimonyController.saveImage,
  function (req, res, next) {
    var errors = validationResult(req);

    // If there are errors in the validation of the form fields, we return the errors to the user
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    } else {
      patrimonyController.edit(req, res);
    }
  }
  // If the image is saved, we call the method to create the patrimony
);

// DELETE a patrimony from the database if the user is logged in and is a manager
router.delete(
  "/delete/:patrimonyId",
  authController.verifyTokenManager,
  // If the user is logged in and is a manager, we delete the patrimony in the url
  patrimonyController.delete
);

module.exports = router;
