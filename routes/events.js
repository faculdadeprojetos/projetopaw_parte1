var express = require("express");
var router = express.Router();
var mongoose = require("mongoose");
var Event = require("../models/Events.js");
var { body, validationResult } = require("express-validator");

var authController = require("../controllers/Auth.js");
var eventController = require("../controllers/Events.js");
const Patrimony = require("../models/Patrimony.js");

router.get("/", authController.verifyToken, eventController.dashboard);

router.get(
  // GET the form to create a new patrimony
  "/formCreate",
  authController.verifyToken,
  eventController.formCreate
);

router.get(
  "/formCreatePatrimonyAssociated/:patrimonyId",
  authController.verifyToken,
  eventController.formCreatePatrimonyAssociated
);

router.get(
  "/create/error",
  authController.verifyToken,
  eventController.formCreateError
);

router.post(
  "/create",
  [
    body("name").isLength({ min: 3, max: 100 }).withMessage("Name is required and must be between 3 and 100 characters"),
    body("description").isLength({ min: 0, max: 750 }).withMessage("Description is not required but must be between 0 and 750 characters"),
    body("type")
      .isIn([
        "Concert",
        "Exhibition",
        "Theater",
        "Festival",
        "Sport",
        "Gallery",
        "Apresentation",
        "Workshop",
        "Religion",
      ]).withMessage("Type is required"),
    body("date").isDate().withMessage("Date is required"),
    body("start").isTime().withMessage("StartTimer is required"),
    body("end").isTime().withMessage("EndTimer is required"),
    body("location").isInt({ min: 1, max: 1000000 }).withMessage("Location is required and must be between 1 and 1000000"),
    body("classification")
      .isIn(["G", "PG", "PG-13", "R", "NC-17"]).withMessage("Classification is required"),
    body("seatsAvailable").isInt({ min: 1, max: 40000 }).withMessage("SeatsAvailable is required and must be between 1 and 40000"),
  ],
  // Verify if the user is logged in
  authController.verifyToken,
  function (req, res, next) {
    var errors = validationResult(req);

    //console.log(req.body);
    if (!errors.isEmpty()) {
      return res.redirect("http://localhost:5000/admin/event/create/error");
    }

    // If there are no errors, we create the event
    eventController.create(req, res);
  }
);

router.get(
  "/complete/:eventId",
  authController.verifyTokenManager,
  eventController.markAsFinished
);

router.get(
  "/getEventById/:eventId",
  authController.verifyToken,
  eventController.getEventById
);

router.get("/show/:eventId", authController.verifyToken, eventController.show);

router.get(
  "/formEdit/:eventId/",
  authController.verifyToken,
  eventController.formEdit
);

router.put(
  "/edit/:eventId",
  [
    body("name").isLength({ min: 3, max: 100 }).withMessage("Name is required and must be between 3 and 100 characters"),
    body("description").isLength({ min: 0, max: 750 }).withMessage("Description is not required but must be between 0 and 750 characters"),
    body("type")
      .isIn([
        "Concert",
        "Exhibition",
        "Theater",
        "Festival",
        "Sport",
        "Gallery",
        "Apresentation",
        "Workshop",
        "Religion",
      ]).withMessage("Type is required"),
    body("date").isDate().withMessage("Date is required"),
    body("start").isTime().withMessage("StartTimer is required"),
    body("end").isTime().withMessage("EndTimer is required"),
    body("location").isInt({ min: 1, max: 1000000 }).withMessage("Location is required and must be between 1 and 1000000"),
    body("classification")
      .isIn(["G", "PG", "PG-13", "R", "NC-17"]).withMessage("Classification is required"),
    body("seatsAvailable").isInt({ min: 1, max: 40000 }).withMessage("SeatsAvailable is required and must be between 1 and 40000"),
  ],
  authController.verifyToken,
  function (req, res, next) {
    var errors = validationResult(req);
    // If there are errors in the validation of the form fields, we return the errors to the user
    //console.log(req.body);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    } else {
      // If there are no errors, we edit the event
      eventController.edit(req, res);
    }
  }
);

// DELETE an event from the database if the user is logged in and is a manager
router.delete(
  "/delete/:eventId",
  // Verify if the user is logged in and is a manager
  authController.verifyTokenManager,
  // Delete the event
  eventController.delete
);

module.exports = router;
