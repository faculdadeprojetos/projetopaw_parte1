var express = require('express');
var router = express.Router();
var salesStoreController = require('../../controllers/store/SalesStore');
var salesController = require('../../controllers/Sales');
var clientStoreController = require('../../controllers/store/ClientStore');

router.post("/registerSale",
    clientStoreController.verifyToken,
    salesStoreController.clientPhase,
    salesController.ticketsPhase,
    salesController.pointsPhase,
    salesController.decisionPhase
);

router.get("/getSalesByClient", clientStoreController.verifyToken, salesStoreController.getSalesByClient);

module.exports = router;