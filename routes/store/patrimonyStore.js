var express = require('express');
var router = express.Router();
var patrimonyStore = require('../../controllers/store/PatrimonyStore');

router.get("/getAllPatrimonies", patrimonyStore.getAllPatrimonies);

module.exports = router;