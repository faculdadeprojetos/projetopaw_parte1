var express = require('express');
var router = express.Router();
var ticketsStore = require('../../controllers/store/TicketsStore');

router.get("/getTicketsForEvent/:eventName", ticketsStore.getTicketsForEvent);

module.exports = router;