var express = require("express");
var router = express.Router();
var clientController = require("../controllers/Client");
var authController = require("../controllers/Auth");
var multer = require("multer");
var { body, validationResult } = require("express-validator");

//GET books dashboard
router.get("/", authController.verifyToken, clientController.dashboard);

//GET the form
router.get(
  "/formCreate",
  authController.verifyToken,
  clientController.formCreate
);

router.get(
  "/create/error",
  authController.verifyToken,
  clientController.formCreateError
);

//POST the submission of client
router.post(
  // POST the form to create a new administrator
  "/create",
  [
    body("name")
      .isLength({ min: 3, max: 60 })
      .matches(/[A-Za-zÀ-ÖØ-öø-ÿ]/)
      .withMessage("Client's name is required!"),
    body("gender")
      .isLength({ min: 1, max: 1 })
      .withMessage("1")
      .isIn(["M", "F", "O", "m", "f", "o"])
      .withMessage("Client's gender (M, F or O) is required!"),
    body("dob")
      .isDate()
      .withMessage("Clients's date of birth (yyyy-mm-dd) is required!"),
    body("cellPhone")
      .matches(/9[1236][0-9]{7}|2[1-9][0-9]{7}/)
      .withMessage("Clients's cell phone (9xxxxxxxx) is required or Clients's cell phone (2xxxxxxxx) is required!"),
    body("email")
      .isLength({ min: 3, max: 70 })
      .isEmail()
      .withMessage("Client's email (xxx@xxx.xxx) is required!"),
    body("password")
      .isLength({ min: 3 })
      .withMessage("Client's password is required!"),
    body("address")
      .isLength({ min: 3, max: 70 })
      .withMessage("Clients's street is required!"),
    body("city")
      .isLength({ min: 3, max: 50 })
      .withMessage("Clients's city is required!"),
    body("postalCode")
      .isPostalCode("any", { locale: "pt-PT" })
      .withMessage("Clients's postal code (xxxx-xxx) is required!"),
    body("nif")
      .isLength(9)
      .withMessage("Client's cell phone (9xxxxxxxx) is required!"),
    body("password")
      .isLength({ min: 3 })
      .withMessage("Password is required!"),
  ],
  // Verify if the user is logged in
  authController.verifyToken,
  function (req, res, next) {
    var errors = validationResult(req);


    // If there are errors in the validation of the form fields, we return the errors to the user
    if (!errors.isEmpty()) {
      return res.redirect("http://localhost:5000/admin/client/create/error");
    }

    // If the user is logged in, we call the method to create the administrator
    clientController.create(req, res);
  }
);

//GET form edit
router.get(
  "/formEdit/:clientId",
  authController.verifyToken,
  clientController.formEdit
);

//GET to edit the client
router.put(
  "/edit/:clientId",
  [
    body("name")
      .isLength({ min: 3, max: 60 })
      .matches(/[A-Za-zÀ-ÖØ-öø-ÿ]/)
      .withMessage("Client's name is required!"),
    body("gender")
      .isLength({ min: 1, max: 1 })
      .withMessage("1")
      .isIn(["M", "F", "O", "m", "f", "o"])
      .withMessage("Client's gender (M, F or O) is required!"),
    body("dob")
      .isDate()
      .withMessage("Clients's date of birth (yyyy-mm-dd) is required!"),
    body("cellPhone")
      .matches(/9[1236][0-9]{7}|2[1-9][0-9]{7}/)
      .withMessage("Clients's cell phone (9xxxxxxxx) is required or Clients's cell phone (2xxxxxxxx) is required!"),
    body("email")
      .isLength({ min: 3, max: 70 })
      .isEmail()
      .withMessage("Client's email (xxx@xxx.xxx) is required!"),
    body("address")
      .isLength({ min: 3, max: 70 })
      .withMessage("Clients's street is required!"),
    body("city")
      .isLength({ min: 3, max: 50 })
      .withMessage("Clients's city is required!"),
    body("postalCode")
      .isPostalCode("any", { locale: "pt-PT" })
      .withMessage("Clients's postal code (xxxx-xxx) is required!"),
    body("nif")
      .isLength(9)
      .withMessage("Client's cell phone (9xxxxxxxx) is required!"),
  ],
  authController.verifyToken,
  function (req, res, next) {
    var errors = validationResult(req);

    // If there are errors in the validation of the form fields, we return the errors to the user
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }

    // If the user is logged in, we call the method to create the administrator
    clientController.edit(req, res);
  }
);

router.get("/getClientById/:clientId", clientController.getClientById);

//GET show client details
router.get(
  "/show/:clientId",
  authController.verifyToken,
  clientController.show
);

//GET to delete one client
router.delete(
  "/delete/:clientId",
  authController.verifyToken,
  clientController.delete
);

module.exports = router;
