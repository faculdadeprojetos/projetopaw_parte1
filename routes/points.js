var express = require("express");
var router = express.Router();
var pointsController = require("../controllers/Points");
var authController = require("../controllers/Auth");
var { body, validationResult } = require("express-validator");

//GET the dasboard with the fidelization program of store if already is one defined
router.get("/", authController.verifyTokenManager, pointsController.dashboard);

//GET the form to add a fidelization program
router.get(
  "/formCreate",
  authController.verifyTokenManager,
  pointsController.formCreate
);

router.get(
  "/create/error",
  authController.verifyTokenManager,
  pointsController.formCreateError
);

//POST add the fidelization program to the database
router.post(
  "/create",
  [
    body("earnedPointsForEachEuro")
      .isNumeric()
      .custom((value) => {
        if (value < 0) {
          throw new Error("The value must be positive");
        }

        return true;
      }),
    body("discountForEach100Points")
      .isNumeric()
      .custom((value) => {
        if (value < 0) {
          throw new Error("The value must be positive");
        }

        return true;
      }),
    body("earnedPointsForEachPurchaseUsedBook")
      .isNumeric()
      .custom((value) => {
        if (value < 0) {
          throw new Error("The value must be positive");
        }

        return true;
      }),
    body("vipTicket")
      .isNumeric()
      .custom((value) => {
        if (value < 0) {
          throw new Error("The value must be positive");
        }

        return true;
      }),
    body("normalTicket")
      .isNumeric()
      .custom((value) => {
        if (value < 0) {
          throw new Error("The value must be positive");
        }

        return true;
      }),
    body("familyTicket")
      .isNumeric()
      .custom((value) => {
        if (value < 0) {
          throw new Error("The value must be positive");
        }

        return true;
      }),
    body("childrenTicket")
      .isNumeric()
      .custom((value) => {
        if (value < 0) {
          throw new Error("The value must be positive");
        }

        return true;
      }),
  ],
  authController.verifyTokenManager,
  function (req, res, next) {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.redirect("http://localhost:5000/admin/point/create/error");
    }

    pointsController.create(req, res, next);
  }
);

//GET the form to edit the atual fidelization program
router.get(
  "/formEdit/:id",
  authController.verifyTokenManager,
  pointsController.formEdit
);

//POST edit the fidelization program
router.put(
  "/edit/:id",
  [
    body("earnedPointsForEachEuro")
      .isNumeric()
      .custom((value) => {
        if (value < 0) {
          throw new Error("The value must be positive");
        }

        return true;
      }),
    body("discountForEach100Points")
      .isNumeric()
      .custom((value) => {
        if (value < 0) {
          throw new Error("The value must be positive");
        }

        return true;
      }),
    body("earnedPointsForEachPurchaseUsedBook")
      .isNumeric()
      .custom((value) => {
        if (value < 0) {
          throw new Error("The value must be positive");
        }

        return true;
      }),
    body("vipTicket")
      .isNumeric()
      .custom((value) => {
        if (value < 0) {
          throw new Error("The value must be positive");
        }

        return true;
      }),
    body("normalTicket")
      .isNumeric()
      .custom((value) => {
        if (value < 0) {
          throw new Error("The value must be positive");
        }

        return true;
      }),
    body("familyTicket")
      .isNumeric()
      .custom((value) => {
        if (value < 0) {
          throw new Error("The value must be positive");
        }

        return true;
      }),
    body("childrenTicket")
      .isNumeric()
      .custom((value) => {
        if (value < 0) {
          throw new Error("The value must be positive");
        }

        return true;
      }),
  ],
  authController.verifyTokenManager,
  function (req, res, next) {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }

    pointsController.edit(req, res, next);
  }
);

//GET delete the atual fidelization program
router.delete(
  "/delete/:id",
  authController.verifyTokenManager,
  pointsController.delete
);

//GET the discount for each 100 points
router.get(
  "/getDiscountForEach100Points",
  authController.verifyToken,
  pointsController.getDiscountForEach100Points
);

module.exports = router;
