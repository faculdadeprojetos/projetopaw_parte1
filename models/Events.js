var mongoose = require("mongoose");

var EventSchema = new mongoose.Schema({
  // Id of the event
  eventId: Number,
  // Name of the event
  name: String,
  // Description of the event
  description: String,
  // Type of the event - concert or exhibition
  type: String,
  // Date of the event
  date: Date,
  // Time of the event
  schedule: { start: String, end: String },
  // Location of the event - Id of the patrimony where the event is going to happen
  location: Number,
  // Classification of the event - G, PG, PG-13, R, NC-17
  classification: String,
  // Type of pricing: free or paid
  seatsAvailable: Number,
  // Boolean to check if the event is finished or not
  finished: { type: Boolean, default: false },
  updated_at: { type: Date, default: Date.now },
});

module.exports = mongoose.model("Event", EventSchema);
