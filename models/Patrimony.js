var mongoose = require('mongoose');

var PatrimonySchema = new mongoose.Schema({
    patrimonyId: Number,
    name: String,
    location: {latitude: Number, longitude: Number, city: String, district: String},
    type: String,
    eventsRelated: {ongoingEvents:[Number], doneEvents:[Number]},
    image: {publicPath: String, typeOfFile: String},
    updated_at: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Patrimony', PatrimonySchema);