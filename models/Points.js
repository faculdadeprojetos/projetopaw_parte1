var mongoose = require("mongoose");

var PointsSchema = new mongoose.Schema(
  {
    earnedPointsForEachEuro: Number,
    freeShipPoints: Number,
    discountForEach100Points: Number,
    earnedPointsForEachPurchaseTicket: Number, //points earned for each Tickets that the client sell to the store
    vipTicket: Number,
    normalTicket: Number,
    childrenTicket: Number,
    familyTicket: Number,
  },
  {
    versionKey: false,
  }
);

module.exports = mongoose.model("Points", PointsSchema);
