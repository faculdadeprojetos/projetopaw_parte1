var mongoose = require("mongoose");

var TicketSchema = new mongoose.Schema({
  // ID of the ticket bought by the user
  ticketId: Number,
  // Name of the event
  name: String,
  // Date of the event (day, month, year)
  date: Date,
  // Hour of the event (hour, minutes)
  hour: String,
  // Seat/Row combination of the ticket bought by the user
  eventSeat: {
    // ID of the row
    row: Number,
    // ID of the seat
    seat: Number,
  },
  // Type of ticket bought by the user - "VIP", "Normal", "Child", "Family"
  type: String,
  // Price of the ticket bought by the user
  price: Number,
  // Image of a QR code that will be used to validate the ticket
  image: {
    // Path of the image in the server
    publicPath: String,
    // Extension of the image - "jpg", "png", "jpeg"
    typeOfFile: String,
  },
  updated_at: { type: Date, default: Date.now },
});

module.exports = mongoose.model("Ticket", TicketSchema);
