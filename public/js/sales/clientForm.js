let clientInfo = document.getElementById("clientInfo");

// Create the rows and columns to display the client info
let row1 = document.createElement("div");
row1.setAttribute("class", "row");
let row2 = document.createElement("div");
row2.setAttribute("class", "row");
let row7 = document.createElement("div");
row7.setAttribute("class", "row");
let col1 = document.createElement("div");
col1.setAttribute("class", "col-lg-4 col-sm-12");
let col2 = document.createElement("div");
col2.setAttribute("class", "col-lg-4 col-sm-12");
let col3 = document.createElement("div");
col3.setAttribute("class", "col-lg-4 col-sm-12");
let col4 = document.createElement("div");
col4.setAttribute("class", "col-lg-4 col-sm-12");
let col5 = document.createElement("div");
col5.setAttribute("class", "col-lg-4 col-sm-12");
let col6 = document.createElement("div");
col6.setAttribute("class", "col-lg-4 col-sm-12");
let col7 = document.createElement("div");
col7.setAttribute("class", "col-lg-4 col-sm-12");
let col8 = document.createElement("div");
col8.setAttribute("class", "col-lg-4 col-sm-12");
let col9 = document.createElement("div");
col9.setAttribute("class", "col-lg-4 col-sm-12");

// Create the labels for the fields
let name = document.createElement("label");
name.innerHTML = "Name";
let gender = document.createElement("label");
gender.innerHTML = "Gender";
let dob = document.createElement("label");
dob.innerHTML = "Date of Birthday";
let cellPhone = document.createElement("label");
cellPhone.innerHTML = "Cell Phone number";
let email = document.createElement("label");
email.innerHTML = "Email";
let address = document.createElement("label");
address.innerHTML = "Address";
let city = document.createElement("label");
city.innerHTML = "City";
let postalCode = document.createElement("label");
postalCode.innerHTML = "Postal Code";
let nif = document.createElement("label");
nif.innerHTML = "NIF";

// Create the input fields
let nameInput = document.createElement("input");
nameInput.setAttribute("type", "text");
nameInput.setAttribute("class", "form-control");
nameInput.setAttribute("id", "nameDisplay");
nameInput.setAttribute("name", "nameDisplay");
nameInput.setAttribute("readonly", "true");
nameInput.setAttribute("required", "true");

let genderInput = document.createElement("input");
genderInput.setAttribute("type", "text");
genderInput.setAttribute("class", "form-control");
genderInput.setAttribute("id", "genderDisplay");
genderInput.setAttribute("name", "genderDisplay");
genderInput.setAttribute("readonly", "true");
genderInput.setAttribute("required", "true");

let dobInput = document.createElement("input");
dobInput.setAttribute("type", "date");
dobInput.setAttribute("class", "form-control");
dobInput.setAttribute("id", "dobDisplay");
dobInput.setAttribute("name", "dobDisplay");
dobInput.setAttribute("readonly", "true");
dobInput.setAttribute("required", "true");

let cellPhoneInput = document.createElement("input");
cellPhoneInput.setAttribute("type", "text");
cellPhoneInput.setAttribute("class", "form-control");
cellPhoneInput.setAttribute("id", "cellPhoneDisplay");
cellPhoneInput.setAttribute("name", "cellPhoneDisplay");
cellPhoneInput.setAttribute("readonly", "true");
cellPhoneInput.setAttribute("required", "true");

let emailInput = document.createElement("input");
emailInput.setAttribute("type", "text");
emailInput.setAttribute("class", "form-control");
emailInput.setAttribute("id", "emailDisplay");
emailInput.setAttribute("name", "emailDisplay");
emailInput.setAttribute("readonly", "true");
emailInput.setAttribute("required", "true");

let addressInput = document.createElement("input");
addressInput.setAttribute("type", "text");
addressInput.setAttribute("class", "form-control");
addressInput.setAttribute("id", "addressDisplay");
addressInput.setAttribute("name", "addressDisplay");
addressInput.setAttribute("readonly", "true");
addressInput.setAttribute("required", "true");

let cityInput = document.createElement("input");
cityInput.setAttribute("type", "text");
cityInput.setAttribute("class", "form-control");
cityInput.setAttribute("id", "cityDisplay");
cityInput.setAttribute("name", "cityDisplay");
cityInput.setAttribute("readonly", "true");
cityInput.setAttribute("required", "true");

let postalCodeInput = document.createElement("input");
postalCodeInput.setAttribute("type", "text");
postalCodeInput.setAttribute("class", "form-control");
postalCodeInput.setAttribute("id", "postalCodeDisplay");
postalCodeInput.setAttribute("name", "postalCodeDisplay");
postalCodeInput.setAttribute("readonly", "true");
postalCodeInput.setAttribute("required", "true");

let nifInput = document.createElement("input");
nifInput.setAttribute("type", "text");
nifInput.setAttribute("class", "form-control");
nifInput.setAttribute("id", "nifDisplay");
nifInput.setAttribute("name", "nifDisplay");
nifInput.setAttribute("readonly", "true");
nifInput.setAttribute("required", "true");

// Append the labels to the columns
col1.appendChild(name);
col2.appendChild(gender);
col3.appendChild(dob);
col4.appendChild(cellPhone);
col5.appendChild(email);
col6.appendChild(address);
col7.appendChild(city);
col8.appendChild(postalCode);
col9.appendChild(nif);

// Append the inputs fields to the columns
col1.appendChild(nameInput);
col2.appendChild(genderInput);
col3.appendChild(dobInput);
col4.appendChild(cellPhoneInput);
col5.appendChild(emailInput);
col6.appendChild(addressInput);
col7.appendChild(cityInput);
col8.appendChild(postalCodeInput);
col9.appendChild(nifInput);

// Append the columns to the rows
row1.appendChild(col1);
row1.appendChild(col2);
row1.appendChild(col3);
row2.appendChild(col4);
row2.appendChild(col5);
row2.appendChild(col6);
row7.appendChild(col7);
row7.appendChild(col8);
row7.appendChild(col9);

// Append the rows to the div that will display the personal information of the client
clientInfo.appendChild(row1);
clientInfo.appendChild(row2);
clientInfo.appendChild(row7);

// Set all child elements of the div to be hidden until the user searches for a client
clientInfo.childNodes.forEach((element) => {
  element.setAttribute("hidden", "true");
});

document.getElementById("btnSearchClient").addEventListener("click", () => {
  var selectElement = document.getElementById("clientChosen");
  var selectedOption = selectElement.options[selectElement.selectedIndex];
  var value = selectedOption.value;
  const url = "/admin/client/getClientById/" + value;

  // Request the client info from the server
  fetch(url).then(async (response) => {
    let responseBody = await response.json();

    document.getElementById("nameDisplay").value =
      responseBody.personalInformation.name;
    document.getElementById("genderDisplay").value =
      responseBody.personalInformation.gender;
    document.getElementById("dobDisplay").value =
      responseBody.personalInformation.dob;
    document.getElementById("cellPhoneDisplay").value =
      responseBody.personalInformation.cellPhone;
    document.getElementById("emailDisplay").value =
      responseBody.personalInformation.email;
    document.getElementById("addressDisplay").value =
      responseBody.personalInformation.address;
    document.getElementById("cityDisplay").value =
      responseBody.personalInformation.city;
    document.getElementById("postalCodeDisplay").value =
      responseBody.personalInformation.postalCode;
    document.getElementById("nifDisplay").value =
      responseBody.personalInformation.nif;

    // Make the div visible
    clientInfo.removeAttribute("hidden");
    clientInfo.childNodes.forEach((element) => {
      element.removeAttribute("hidden");
    });
  });
});
