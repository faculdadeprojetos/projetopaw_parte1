// Create the button to "submit" the form
let submitButton = document.createElement("button");
// When the button is clicked, we call a function that will
// filter the input fields and reorganize them into a JSON
// that the server can understand
// This is done because the server expects a JSON with the
// amount of tickets for each type of ticket
// as an array of objects

submitButton.addEventListener("click", () => {
  // Call the function that will filter the input fields
  // and reorganize them into a JSON

  let ObjectToSend = new Map();
  ObjectToSend.set("vipTickets", parseInt(document.getElementById("vipTicket").value));
  ObjectToSend.set(
    "familyTickets",
    parseInt(document.getElementById("familyTicket").value)
  );
  ObjectToSend.set(
    "normalTickets",
    parseInt(document.getElementById("normalTicket").value)
  );
  ObjectToSend.set(
    "childTickets",
    parseInt(document.getElementById("childTicket").value)
  );
  ObjectToSend.set(
    "totalTickets",
    parseInt(document.getElementById("vipTicket").value) +
    parseInt(document.getElementById("familyTicket").value) +
    parseInt(document.getElementById("normalTicket").value) +
    parseInt(document.getElementById("childTicket").value)
  );
  ObjectToSend.set(
    "eventName",
    document.getElementById("eventChosen").options[
      document.getElementById("eventChosen").selectedIndex
    ].innerText
  );
  ObjectToSend.set("clientNif", document.getElementById("nifDisplay").value);

  if (document.getElementById("hasAppliedDiscount").checked) {
    ObjectToSend.set("hasAppliedDiscount", true);
  } else {
    ObjectToSend.set("hasAppliedDiscount", false);
  }


  //console.log(ObjectToSend);
  // Send the JSON to the server
  const xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      let response = JSON.parse(this.responseText);
      let salesId = response.salesId;
      //console.log("Sale registered successfully");
      // If the server returns a 200 status, it means that the
      // sale was registered successfully
      // We then redirect the user to the sale that was just created
      window.location.href = "/admin/sale/show/" + salesId;
    } else if (this.readyState == 4 && this.status == 409) {
      // Not enough seats available error message
      document.getElementById("createError").hidden = false;
      window.scrollTo(0, 0);
    } else {
      document.getElementById("createErrorGeneric").hidden = false;
      window.scrollTo(0, 0);
    }
  };
  xhttp.open("POST", "/admin/sale/create");
  xhttp.setRequestHeader("Content-Type", "application/json");
  xhttp.send(JSON.stringify(Object.fromEntries(ObjectToSend)));
});
submitButton.setAttribute("type", "button");
submitButton.setAttribute("class", "btn btn-primary");
submitButton.setAttribute("style", " width: 150px");
submitButton.setAttribute("id", "btnSubmit");
submitButton.innerText = "Submit Sale";

var hasAppliedDiscount = document.createElement("input");
hasAppliedDiscount.setAttribute("type", "checkbox");
hasAppliedDiscount.setAttribute("id", "hasAppliedDiscount");
hasAppliedDiscount.setAttribute("name", "hasAppliedDiscount");
hasAppliedDiscount.setAttribute("value", "hasAppliedDiscount");
hasAppliedDiscount.setAttribute("class", "form-check-input");
hasAppliedDiscount.setAttribute("style", "margin-top: 12px");

var hasAppliedDiscountLabel = document.createElement("label");
hasAppliedDiscountLabel.setAttribute("for", "hasAppliedDiscount");
hasAppliedDiscountLabel.setAttribute("class", "form-check-label");
hasAppliedDiscountLabel.setAttribute("style", "padding-top: 6px");
hasAppliedDiscountLabel.innerText = "Apply Discount";

const observer2 = new MutationObserver((mutationsList, observer2) => {
  if (
    !ticketInfo.hasAttribute("hidden") &&
    !clientInfo.hasAttribute("hidden")
  ) {
    document.getElementById("saleDiscount").appendChild(hasAppliedDiscount);
    document.getElementById("saleDiscount").appendChild(hasAppliedDiscountLabel);
    document.getElementById("saleContainer").appendChild(submitButton);
    observer2.disconnect();
  }
});
// how does observer2 observe both ticketInfo and clientInfo?

observer2.observe(clientInfo, { attributes: true });
observer2.observe(ticketInfo, { attributes: true });
